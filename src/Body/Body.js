import React, { Component } from 'react';
import coachingForLeaders from './coaching-for-leaders.png';
import squigglyCareers from './squiggly-careers.jpg';
import theChangeLog from './the-change-log.png';
import frontEndHappyHour from './front-end-happy-hour.jpg';
import './Body.css';

function Row(props) {
  return (
    <div>
      {props.podcasts.map((podcast) =>
        <div className={podcast.id % 2 ? 'row reverse' : 'row'} key={podcast.id}>
          <div>
            <img src={podcast.img} alt={podcast.title} />
          </div>

          <div>
            <p>{podcast.quote}</p>
            <a href={podcast.url}>Listen on Spotify</a>
          </div>
        </div>
      )}
    </div>
  )
}

const podcasts = [
  {
    id: 1,
    title: 'Coaching for leaders',
    img: coachingForLeaders,
    quote: '"Coaching for Leaders is my go-to listen during my daily commute. The topics and the individuals interviewed always have something to share that resonates with me. Dave provides content that is timely, relevant, and thought-provoking. Not only do I consider Coaching for Leaders my own personal development tool, I also recommend it to everyone I know. It is a tool that any professional and aspiring leader needs to have in their pocket."',
    url: 'https://open.spotify.com/show/3ChDtTY9JyupfNGxcV39b3?si=17CBGePTRw-bygL4LCTWjA'
  },
  {
    id: 2,
    title: 'Squiggly Careers',
    img: squigglyCareers,
    quote: '"Full of tips, tools and techniques that you can put into practice straight away. Hosted by the founders of Amazing If, Sarah Ellis and Helen Tupper, whose courses and coaching have helped 1000s of people have a happier career."',
    url: 'https://open.spotify.com/show/6t6vYEeYl3Kmdd0AE6v3xj?si=Nk4EUqR-ReKUoZq6u8pT8Q'
  },
  {
    id: 3,
    title: 'Front End Happy Hour',
    img: frontEndHappyHour,
    quote: '"A great podcast featuring a panel of Software Engineers from Netflix, Evernote, Atlassian & LinkedIn talking over drinks about all things Front End development."',
    url: 'https://open.spotify.com/show/0Giuw6eNbTzP9CDZODDrA2?si=PP8qaX5RSUS6gUkXgRHezg'
  },
  {
    id: 4,
    title: 'The Change Log',
    img: theChangeLog,
    quote: '"A great podcast to keep up with the latest news in development."',
    url: 'https://open.spotify.com/show/5bBki72YeKSLUqyD94qsuJ?si=CRBNoE_1Qd2BD6UkqoOJLw'
  }
];

class Body extends Component {
  render() {
    return (
      <div className="email">
        <h2>We can't wait for you to join the band as an <span className="emphasise">Engineering Manager</span> and thought you may like to give these a listen!</h2>
        <h3>We know it can sometimes be tricky to keep up to date with things in the world of development, so here are some great ones to help with that...</h3>

        <Row podcasts={podcasts} />
      </div>
    )
  }
}

export default Body;
