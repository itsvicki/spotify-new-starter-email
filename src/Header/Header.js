import React, { Component } from 'react';
import logo from './logo-white.png';
import './Header.css';

class Header extends Component {
  render() {
    return (
      <div className="header">
        <img className="logo" src={logo} alt="Spotify" />

        <h1>2 weeks to go!</h1>
      </div>
    )
  }
}

export default Header;
